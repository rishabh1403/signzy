var express = require('express');
var app = express();
var request = require('request');
var bodyParser = require('body-parser');

app.use(express.static(__dirname + '/public'));
// parse application/x-www-form-urlencoded 
app.use(bodyParser.urlencoded({
    extended: true
}));

// parse application/json 
app.use(bodyParser.json());

app.post('/uber/estimate', function (req, res) {
    console.log(req.body);
    request.get({
        headers: {
            Authorization: "Token " + "ECWcv5urK26d-pz-OHio9c9ovHpahx4UBbQIzMTi",
        },
        url: "https://api.uber.com/v1/estimates/time?start_latitude=" + req.body.start_latitude + "&start_longitude=" + req.body.start_longitude,
    }, function (error, response, body) {
        //console.log(response.body);
        //console.log(error);
        var arrData = JSON.parse(response.body).times;
        //console.log(arrData);
        //console.log(arrData[1]);
        res.send(arrData[1]);
    });
})



app.listen(5000, function (err) {
    if (err)
        console.log(err);
    else
        console.log("Server started on http://localhost:5000");
})