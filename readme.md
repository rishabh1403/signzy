# Signzy Assignment

## Run the Application
* clone the repo.
* cd into Signzy folder and run **npm install**.
* run **node server.js** in your terminal.
* visit http://localhost:5000 in your browser.

## Frameworks and libraries used
* *ExpressJS*
* *Jquery*(for easy value extraction from input fields)
* *Moment.js* for easy time handling
* *materialize* ( for a bit smooth material design ui)

## Apis consumed
* Google Maps Apis
* Uber Apis

## Approach towards solving the problem.

> In the index html file, there is different input fields for different inputs.
> Once the user clicks the set reminder button the respective click handler is triggered.
> Inside click handler we extract source and destination locations, generate a nice time string and call google map api function.
> Inside gmap function distance matrix api is called and duration of travel is extracted (in seconds) and stored in variables.
> All the global variables are used to reduce the parmeters passed to every function, thus reducing chances of bugs complaining for undefined variables.
> After the results are parsed, uber api function is called, it gets pickup ETA and stores it, thus calling the calculate function.
> All the major logic is stored here.
> It gets current time difference from destination time, calcute time for user to reach the destination from map data and uber data.
> If time to reach is greater than current time difference, user is notified to leave, while any interval or timeout functions are cleared.
> If that is not the case we calculate the worst case scenario time and if that is greater than current time difference, we attach a setInterval function to do all the steps again every minute.
> Else we attach a setTimeout function to start all these above mentioned steps once we reach worst case time threshold.
> So, the process keeps on continuing recursively till user is notified and all timers are cleared.

