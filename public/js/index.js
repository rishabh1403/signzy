$(function () {
    var globalObj = {
        sourceLat: null,
        sourceLong: null,
        destLat: null,
        destLong: null,
        destTime: null,
        email: null,
        pickupETA: 0, //seconds eta to pickup
        gmapETA: 0, //seconds of travel
        myInterval: null,
        myTimeout: null,
        $successSvg: ' <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"' +
            'viewBox="0 0 507.2 507.2" style="enable-background:new 0 0 507.2 507.2;width:30px;height:30px;" xml:space="preserve">' +
            '<circle style="fill:#32BA7C;" cx="253.6" cy="253.6" r="253.6"/>' +
            '<path style="fill:#0AA06E;" d="M188.8,368l130.4,130.4c108-28.8,188-127.2,188-244.8c0-2.4,0-4.8,0-7.2L404.8,152L188.8,368z"/>' +
            '<g><path style="fill:#FFFFFF;" d="M260,310.4c11.2,11.2,11.2,30.4,0,41.6l-23.2,23.2c-11.2,11.2-30.4,11.2-41.6,0L93.6,272.8' +
            '		c-11.2-11.2-11.2-30.4,0-41.6l23.2-23.2c11.2-11.2,30.4-11.2,41.6,0L260,310.4z"/>' +
            '	<path style="fill:#FFFFFF;" d="M348.8,133.6c11.2-11.2,30.4-11.2,41.6,0l23.2,23.2c11.2,11.2,11.2,30.4,0,41.6l-176,175.2' +
            '		c-11.2,11.2-30.4,11.2-41.6,0l-23.2-23.2c-11.2-11.2-11.2-30.4,0-41.6L348.8,133.6z"/>' +
            '</g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>',
        getDistanceMatrixData: function () {
            var that = this;
            var origin = new google.maps.LatLng(this.sourceLat, this.sourceLong);
            var destination = new google.maps.LatLng(this.destLat, this.destLong);
            var service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix({
                origins: [origin],
                destinations: [destination],
                travelMode: 'DRIVING'
            }, callback);

            function callback(response, status) {
                that.gmapETA = response.rows[0].elements[0].duration.value;
                // store time here
                //call uber api.
                that.getUberTimeEstimate();
            }
        },
        getUberTimeEstimate: function () {
            var that = this;
            $.post(
                "/uber/estimate", {
                    "start_latitude": 12.9166,
                    "start_longitude": 77.6101
                },
                function (result) {
                    console.log(parseInt(result.estimate));
                    that.pickupETA = parseInt(result.estimate);
                    that.calculateDifferenceAndRemind();
                    //store time for uber
                    //call calculate difference and remind
                }
            );

        },
        calculateDifferenceAndRemind: function () {
            //get current time
            var starthour = null;
            var mString = 'am';
            var d = new Date();
            var startMinutes = d.getMinutes();
            if (d.getHours() > 12) {
                starthour = d.getHours() - 12;
                mString = "pm";
            } else {
                starthour = d.getHours();
            }
            var startTime = moment(starthour + ":" + startMinutes + ":00 " + mString, 'hh:mm:ss a');
            var endTime = moment(this.destTime, 'hh:mm:ss a');
            var totalHours = (endTime.diff(startTime, 'hours'));
            var totalMinutes = endTime.diff(startTime, 'minutes');
            var clearMinutes = totalMinutes % 60;
            var totalSeconds = totalMinutes * 60 * 60 + clearMinutes * 60;
            console.log(totalHours + " hours and " + clearMinutes + " minutes");
            //find difference in seconds to destTime
            //calcute reach time pickup+gmap
            var timeToReach = this.pickupETA + this.gmapETA;
            if (timeToReach >= totalSeconds) {
                this.remindUser();
                clearInterval(this.myInterval);
                clearTimeout(this.myTimeout);
            } else {
                var worstTime = this.gmapETA + 3600 + 900; //60min dev og gmap and 15 min capped pickup eta
                var that = this;
                if (totalSeconds <= worstTime) {
                    // check every 1 minute;
                    myInterval = setInterval(function () {
                        that.getDistanceMatrixData();
                    }, 60000);
                } else {
                    //check from worstTime
                    myTimeout = setTimeout(function () {
                        that.getDistanceMatrixData();
                    }, (totalSeconds - worstTime) * 1000);
                }
            }
            //if recah <= diff remind
            //else calcute worst time
            //if great worst time set timeout
            //else set interval;
        },
        handleClickOnSetReminderButton: function () {
            var that = this;
            console.log("here hab");
            $('#setReminder').click(function () {
                $('.scale-transition').fadeOut(1000);
                setTimeout(function () {
                    $('.feedback').fadeIn('slow');

                }, 1500);
                that.sourceLat = $('#startLat').val();
                that.sourceLong = $('#startLong').val();
                that.destLat = $('#endLat').val();
                that.destLong = $('#endLong').val();
                var str = 'am';
                var dhour = $('#destHour').val();
                var dmin = $('#destMin').val();
                if (dhour > 12) {
                    str = 'pm',
                        dhour = dhour - 12;
                }
                that.destTime = dhour + ":" + dmin + ":00 " + str;
                that.email = $('#email');
                that.getDistanceMatrixData();
            });
        },
        registerClickHandlers: function () {
            this.handleClickOnSetReminderButton();
        },
        remindUser: function () {
            //call server to remind user of the cab book arrival
            //pass the user email id
        },
        init: function () {
            this.registerClickHandlers();
        }
    };
    globalObj.init();
})